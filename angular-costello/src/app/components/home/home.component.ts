import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public message = "Hi Team Costello! Thank you for looking! :~)";

  public myFunc() {
    alert(this.message);
  }

  public type_text: string = "Powerful insights into your team";
  public type_display: string = "";

  typeCallback(that) {
    let total_length = that.type_text.length;
    let current_length = that.type_display.length;
    if (current_length < total_length) {
      that.type_display += that.type_text[current_length];
      setTimeout(that.typeCallback, 50, that);
    } else {
      that.type_display = "Powerful insights into your team";
    }
  }

  constructor() { }

  ngOnInit(): void {

    this.typeCallback(this);

  }

}
